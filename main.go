package main

import (
	"log"
	"net/http"
	"os"
	"poc-backend-service/handler"

	"github.com/joho/godotenv"
	"github.com/xanzy/go-gitlab"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	// create a new gitlab client
	git, err := gitlab.NewClient(os.Getenv("GITLAB_AUTH_TOKEN"), gitlab.WithBaseURL(os.Getenv("GITLAB_BASE_URL")))

	http.HandleFunc("/create-test-production", handler.CreateTestProductionHandler(git))
	http.HandleFunc("/create-request", handler.CreateRequestHandler)
	http.ListenAndServe(":8080", nil)
}
