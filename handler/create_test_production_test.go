package handler

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_labelTitleExists(t *testing.T) {
	testCases := []struct {
		name     string
		data     map[string]interface{}
		expected bool
	}{
		{
			name: "title exists",
			data: map[string]interface{}{
				"changes": map[string]interface{}{
					"labels": map[string]interface{}{
						"current": []interface{}{
							map[string]interface{}{
								"title": "label title",
							},
						},
					},
				},
			},
			expected: true,
		},
		{
			name: "title does not exist",
			data: map[string]interface{}{
				"changes": map[string]interface{}{
					"labels": map[string]interface{}{
						"current": []interface{}{
							map[string]interface{}{},
						},
					},
				},
			},
			expected: false,
		},
		{
			name:     "no data",
			data:     map[string]interface{}{},
			expected: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			assert.Equal(t, tc.expected, labelTitleExists(tc.data))
		})
	}
}
