FROM golang:latest

WORKDIR /app

COPY go.mod .
COPY go.sum .
COPY .env.example .env

RUN go mod download

COPY . .

EXPOSE 8080

RUN mkdir -p /.cache/go-build
RUN chmod -R 777 /.cache/go-build
RUN chmod -R a+rwx ./

ENTRYPOINT ["go", "run", "main.go"]