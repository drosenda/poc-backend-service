package handler

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	"github.com/xanzy/go-gitlab"
)

// wrapper function to use gitlab client as singleton
func CreateTestProductionHandler(git *gitlab.Client) http.HandlerFunc {
	// CreateTestProductionHandler handles creating a new branch, adding a file to it, and creating a merge request
	// NOTE: 206 PARTIAL CONTENT status codes are returned for errors because otherwise the GitLab webhook will disable automatically
	return func(w http.ResponseWriter, r *http.Request) {
		// check if the request contains the correct secret token
		if r.Header.Get("X-Gitlab-Token") != os.Getenv("GITLAB_WEBHOOK_SECRET_TOKEN") {
			setResponse("Unauthorized request", w, http.StatusUnauthorized)
			return
		}

		// read the body of the request
		body, err := ioutil.ReadAll(r.Body)
		if checkError(err, w, http.StatusPartialContent) {
			return
		}

		// unmarshal the json data
		var data map[string]interface{}
		err = json.Unmarshal(body, &data)
		if checkError(err, w, http.StatusPartialContent) {
			return
		}

		if !labelTitleExists(data) {
			setResponse("Invalid request. The JSON data does not contain the expected fields.", w, http.StatusPartialContent)
			return
		}

		// check if the json data contains a label title field with the value of "Approved"
		changes := data["changes"].(map[string]interface{})
		labels := changes["labels"].(map[string]interface{})
		current := labels["current"].([]interface{})

		if !contains(current, "Approved") {
			setResponse("This is not an approved request.", w, http.StatusPartialContent)
			return
		}

		// get the latest commit on master
		masterCommit, _, err := git.Commits.GetCommit(os.Getenv("GITLAB_PROJECT_ID"), "master", nil)
		if checkError(err, w, http.StatusPartialContent) {
			return
		}

		// create a new branch
		branchName := fmt.Sprintf("%d", time.Now().UnixNano())

		newBranch := &gitlab.CreateBranchOptions{
			Branch: gitlab.String(branchName),
			Ref:    gitlab.String(masterCommit.ID),
		}

		_, _, err = git.Branches.CreateBranch(os.Getenv("GITLAB_PROJECT_ID"), newBranch)
		if checkError(err, w, http.StatusPartialContent) {
			return
		}

		// create a new file
		yamlContent := `name: value`

		err = writeFile("my-file.yaml", []byte(yamlContent))
		if checkError(err, w, http.StatusPartialContent) {
			return
		}

		// commit the file to the new branch
		commitOptions := &gitlab.CreateCommitOptions{
			Branch:        gitlab.String(branchName),
			CommitMessage: gitlab.String("Committing my-file.yaml"),
			Actions: []*gitlab.CommitActionOptions{
				{
					Action:   gitlab.FileAction(gitlab.FileCreate),
					FilePath: gitlab.String("my-file.yaml"),
					Content:  gitlab.String(yamlContent),
				},
			},
		}

		_, _, err = git.Commits.CreateCommit(os.Getenv("GITLAB_PROJECT_ID"), commitOptions)
		if checkError(err, w, http.StatusPartialContent) {
			return
		}

		// delete the file from the system
		err = os.Remove("my-file.yaml")
		if checkError(err, w, http.StatusPartialContent) {
			return
		}

		// create a merge request
		mrOptions := &gitlab.CreateMergeRequestOptions{
			SourceBranch: gitlab.String(branchName),
			TargetBranch: gitlab.String("master"),
			Title:        gitlab.String("Merging my-file.yaml"),
			Description:  gitlab.String("This MR adds my-file.yaml to the project."),
		}

		_, _, err = git.MergeRequests.CreateMergeRequest(os.Getenv("GITLAB_PROJECT_ID"), mrOptions)
		if checkError(err, w, http.StatusPartialContent) {
			return
		}

		setResponse("Created merge request", w, http.StatusOK)
	}

}

// This function checks whether there is an error
func checkError(err error, w http.ResponseWriter, statusCode int) bool {
	if err != nil {
		setResponse(err.Error(), w, statusCode)
		return true
	}

	return false
}

// Sets the http response with a given error message and status code
func setResponse(errorMessage string, w http.ResponseWriter, statusCode int) {
	fmt.Println(errorMessage)
	w.WriteHeader(statusCode)
	w.Write([]byte(errorMessage))
}

// labelTitleExists checks if the JSON data contains the necessary fields
func labelTitleExists(data map[string]interface{}) bool {
	_, ok := data["changes"]
	if ok {
		changes := data["changes"].(map[string]interface{})
		_, ok = changes["labels"]
		if ok {
			labels := changes["labels"].(map[string]interface{})
			_, ok = labels["current"]
			if ok {
				current := labels["current"].([]interface{})
				if len(current) > 0 {
					_, ok = current[0].(map[string]interface{})
					if ok {
						label := current[0].(map[string]interface{})
						_, ok = label["title"]
						return ok
					}
				}
				return true
			}
		}
	}
	return false
}

// writeFile writes the content to the specified filename
func writeFile(filename string, content []byte) error {
	return ioutil.WriteFile(filename, content, 0644)
}

// This function checks if a given string is present in an array of interfaces.
func contains(arr []interface{}, title string) bool {
	for _, element := range arr {
		label := element.(map[string]interface{})
		if label["title"].(string) == title {
			return true
		}
	}
	return false
}
