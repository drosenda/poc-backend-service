package handler

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)

type IssueRequest struct {
	Title       string `json:"title"`
	Description string `json:"description"`
}

func CreateRequestHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		body, _ := ioutil.ReadAll(r.Body)
		var request IssueRequest
		err := json.Unmarshal(body, &request)
		if err != nil {
			http.Error(w, "Invalid JSON data", http.StatusBadRequest)
			return
		}
		createIssue(request.Title, request.Description)
		fmt.Fprintf(w, "Successfully created issue")
	} else {
		fmt.Fprintf(w, "Only POST requests are supported")
	}
}

func createIssue(title string, description string) {
	token := os.Getenv("GITLAB_AUTH_TOKEN")

	projectID := os.Getenv("GITLAB_PROJECT_ID")

	url := fmt.Sprintf("https://gitlab.cern.ch/api/v4/projects/%s/issues?private_token=%s", projectID, token)

	payload := strings.NewReader(fmt.Sprintf(`{"title":"%s", "description":"%s"}`, title, description))

	req, _ := http.NewRequest("POST", url, payload)

	req.Header.Add("Content-Type", "application/json")

	res, _ := http.DefaultClient.Do(req)

	fmt.Println(res)

	if res.StatusCode == 200 || res.StatusCode == 201 {
		fmt.Println("Issue successfully created!")
	} else {
		fmt.Println("Issue creation failed!")
	}

	defer res.Body.Close()
}
