# LHCb Ntupling Service

![pipeline](https://gitlab.cern.ch/drosenda/poc-backend-service/badges/master/pipeline.svg) ![coverage](https://gitlab.cern.ch/drosenda/poc-backend-service/badges/master/coverage.svg)

## Description

LHCb Ntupling Service is a REST API for managing LHCb Ntuple requests.

### Related repositories

- Frontend: https://gitlab.cern.ch/cernopendata/lhcb-ntupling-service-frontend
- Requests: https://gitlab.cern.ch/cernopendata/lhcb-ntupling-service-requests
- AnalysisProductions: https://gitlab.cern.ch/lhcb-datapkg/AnalysisProductions

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Contributing](#contributing)
- [Tests](#tests)
- [Questions](#questions)
- [License](#license)

## Installation

Install Go: https://go.dev/doc/install

Run the following command to install the dependencies:

```bash
go get
```

## Usage

1. Create .env file and set variables

2. Install [air](https://github.com/cosmtrek/air) (runs the application with hot reload)

```bash
go install github.com/cosmtrek/air@latest
```

3. Run the project:

```bash
air
```

### Example request

This endpoint checks if a request is approved and creates a merge request which spawns a test production

- Method: POST
- Endpoint URL: `http://localhost:8080/create-test-production`
- Header: `X-Gitlab-Token: <TOKEN>`
- Body:

```json
{
  "object_kind": "issue",
  "event_type": "issue",
  "changes": {
    "labels": {
      "previous": [],
      "current": [
        {
          "id": 46166,
          "title": "Running",
          "color": "#009966",
          "project_id": 158050,
          "created_at": "2023-04-03 16:14:53 +0200",
          "updated_at": "2023-04-03 16:14:53 +0200",
          "template": false,
          "description": "",
          "type": "ProjectLabel",
          "group_id": null
        },
        {
          "id": 46166,
          "title": "Approved",
          "color": "#009966",
          "project_id": 158050,
          "created_at": "2023-04-03 16:14:53 +0200",
          "updated_at": "2023-04-03 16:14:53 +0200",
          "template": false,
          "description": "",
          "type": "ProjectLabel",
          "group_id": null
        }
      ]
    }
  }
}
```

## Tests

To run tests, run the following command:

```bash
go test -v -coverpkg=./... -coverprofile=profile.cov ./...
```

To see test coverage per function, run the following command:

```bash
go tool cover -func profile.cov
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change. Please make sure to update tests as appropriate.

## Questions

If you have any questions about the repo, open an issue or contact opendata-support@cern.ch.

## License

This project is licensed under the MIT license.
